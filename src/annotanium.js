(function(root) {
  'use strict';

  var support = {
    dom: true
  };

  // Utilities
  // ==================================================

  var PIXEL_RATIO = (function () {
    try {
      var ctx = document.createElement('canvas').getContext('2d');
      var dpr = window.devicePixelRatio || 1;
      var bsr = ctx.webkitBackingStorePixelRatio ||
                ctx.mozBackingStorePixelRatio    ||
                ctx.msBackingStorePixelRatio     ||
                ctx.oBackingStorePixelRatio      ||
                ctx.backingStorePixelRatio       || 1;

      return dpr / bsr;
    } catch (e) {
      support.dom = false;
    }
  })();

  var utils = {

    createHiDPICanvas: function createHiDPICanvas(w, h, ratio) {
      if (!ratio) ratio = PIXEL_RATIO;

      var cnvs = document.createElement('canvas');

      cnvs.width        = w * ratio;
      cnvs.height       = h * ratio;
      cnvs.style.width  = w + 'px';
      cnvs.style.height = h + 'px';

      cnvs.getContext('2d').setTransform(ratio, 0, 0, ratio, 0, 0);

      return cnvs;
    },

    getDevicePixelRatio: function getDevicePixelRatio() {
      return PIXEL_RATIO;
    },

    wrapHtml: function(wrapperNode, innerNodes) {
      if (!innerNodes.length) innerNodes = [innerNodes];

      innerNodes.reverse().forEach(function(el, i) {
        var child   = (i > 0) ? wrapperNode.cloneNode(true) : wrapperNode;
        var parent  = el.parentNode;
        var sibling = el.nextSibling;

        child.appendChild(el);

        if (sibling) {
          parent.insertBefore(child, sibling);
        } else {
          parent.appendChild(child);
        }
      });
    }

  };


  // Annotation class
  // ==================================================

  var Annotation = (function() {

    function Annotation(context) {
      this.ctx        = context;
      this.cnvsWidth  = context.canvas.clientWidth;
      this.cnvsHeight = context.canvas.clientHeight;
      this.listenersMap = {};
      this.text = null;
    }

    Annotation.prototype.setMeta = function setMeta(metaObj) {
      this._meta = metaObj;
    };

    Annotation.prototype.setRelPositionDimensions = function setRelPositionDimensions(xPos, yPos, width, height) {
      this.xPos     = parseFloat(xPos);
      this.yPos     = parseFloat(yPos);
      this.width    = parseFloat(width);
      this.height   = parseFloat(height);

      this.xPosPx   = xPos   * this.cnvsWidth;
      this.yPosPx   = yPos   * this.cnvsHeight;
      this.widthPx  = width  * this.cnvsWidth;
      this.heightPx = height * this.cnvsHeight;
    };

    Annotation.prototype.setPxPositionDimensions = function setPxPositionDimensions(xPosPx, yPosPx, widthPx, heightPx) {
      this.xPosPx   = parseFloat(xPosPx);
      this.yPosPx   = parseFloat(yPosPx);
      this.widthPx  = parseFloat(widthPx);
      this.heightPx = parseFloat(heightPx);

      this.xPos   = xPosPx   / this.cnvsWidth;
      this.yPos   = yPosPx   / this.cnvsHeight;
      this.width  = widthPx   / this.cnvsWidth;
      this.height = heightPx / this.cnvsHeight;
    };

    Annotation.prototype.setText = function setText(text) {
      this.text = text;

      var textNode = document.createElement('p');
      textNode.className = 'annotanium-annotationText';
      textNode.textContent = text;

      var deleteBtn = document.createElement('button');
      deleteBtn.className = 'annotanium-btn annotanium-btnDelete';
      deleteBtn.textContent = 'Delete';
      deleteBtn.onclick = function() {
        this.emit('annotation.delete', this);
      }.bind(this);

      var popoverContent = document.createElement('div');
      popoverContent.appendChild(textNode);
      popoverContent.appendChild(deleteBtn);

      this.textPopover = new Popover(popoverContent);

      this.ctx.canvas.parentNode.appendChild(this.textPopover.getDOMNode());
      this.textPopover.setPosition(this.xPosPx, this.yPosPx + this.heightPx);
    };

    Annotation.prototype.fromMouseEvents = function fromMouseEvents(mouseStart, mouseEnd) {
      var xPosPx   = Math.min(mouseStart.offsetX, mouseEnd.offsetX);
      var yPosPx   = Math.min(mouseStart.offsetY, mouseEnd.offsetY);
      var widthPx  = Math.abs(mouseEnd.offsetX - mouseStart.offsetX);
      var heightPx = Math.abs(mouseEnd.offsetY - mouseStart.offsetY);

      this.setPxPositionDimensions(xPosPx, yPosPx, widthPx, heightPx);
    };

    Annotation.prototype.setId = function(id) {
      id = id || [this.xPosPx, this.yPosPx, this.widthPx, this.heightPx].join('-');
      this.id = id;
    };

    Annotation.prototype.destroy = function destroy() {
      this.ctx.canvas.parentNode.removeChild(this.textPopover.getDOMNode());
    };

    Annotation.prototype.draw = function draw() {
      this.ctx.strokeRect(this.xPosPx, this.yPosPx, this.widthPx, this.heightPx);
    };

    Annotation.prototype.isMouseOver = function isMouseOver(mouseEvent) {
      var inXBounds = mouseEvent.offsetX >= this.xPosPx &&
        mouseEvent.offsetX <= this.xPosPx + this.widthPx;
      var inYBounds = mouseEvent.offsetY >= this.yPosPx &&
        mouseEvent.offsetY <= this.yPosPx + this.heightPx;

      return inXBounds && inYBounds;
    };

    Annotation.prototype.hideTextPopover = function hideTextPopover() {
      this.textPopover.hide();
    };

    Annotation.prototype.showTextPopover = function showTextPopover() {
      this.textPopover.show();
    };

    // TODO: DRY up event methods
    Annotation.prototype.on = function on(eventName, listener) {
      this.listenersMap[eventName] = this.listenersMap[eventName] || [];
      this.listenersMap[eventName].push(listener);
    };

    Annotation.prototype.off = function off(eventName, listener) {
      var listeners = this.listenersMap[eventName];
      var index = listeners.indexOf(listener);
      if (index > -1) listeners.splice(index, 1);
    };

    Annotation.prototype.emit = function emit(eventName, arg) {
      if (this.listenersMap[eventName]) {
        this.listenersMap[eventName].forEach(function(listener) {
          listener(arg);
        });
      }
    };

    return Annotation;

  }).call(this);


  // AddAnnotationForm class
  // ==================================================

  var AddAnnotationForm = (function() {

    function AddAnnotationForm() {
      this.form                  = document.createElement('form');
      this.form.className        = 'annotanium-addAnnotationForm';

      this.label                 = document.createElement('label');
      this.label.className       = 'annotanium-addAnnotationForm-label';
      this.label.textContent     = 'Write an annotation';

      this.textarea              = document.createElement('textarea');
      this.textarea.className    = 'annotanium-addAnnotationForm-textarea';

      this.submitBtn             = document.createElement('button');
      this.submitBtn.className   = 'annotanium-btn annotanium-btnSubmit';
      this.submitBtn.type        = 'submit';
      this.submitBtn.textContent = 'Add';

      this.cancelBtn             = document.createElement('button');
      this.cancelBtn.className   = 'annotanium-btn annotanium-btnCancel';
      this.cancelBtn.textContent = 'Cancel';

      this.form.appendChild(this.label);
      this.form.appendChild(this.textarea);
      this.form.appendChild(this.submitBtn);
      this.form.appendChild(this.cancelBtn);
    }

    AddAnnotationForm.prototype.focus = function focus() {
      this.textarea.focus();
    };

    AddAnnotationForm.prototype.getDOMNode = function getDOMNode() {
      return this.form;
    };

    AddAnnotationForm.prototype.getValue = function getValue() {
      return this.textarea.value;
    };

    AddAnnotationForm.prototype.onSubmit = function onSubmit(callback) {
      this.form.onsubmit = callback;
    };

    AddAnnotationForm.prototype.onCancel = function onCancel(callback) {
      this.cancelBtn.onclick = callback;
    };

    AddAnnotationForm.prototype.reset = function reset() {
      this.textarea.value = '';
    };

    return AddAnnotationForm;

  }).call(this);


  // Popover class
  // ==================================================

  var Popover = (function() {

    function Popover(content) {
      this.domNode = document.createElement('div');
      this.domNode.className = 'annotanium-popover';
      this.domNode.appendChild(content);
    }

    Popover.prototype.getDOMNode = function getDOMNode() {
      return this.domNode;
    };

    Popover.prototype.setPosition = function setPosition(x, y) {
      this.domNode.setAttribute('style', 'left:' + x + 'px;top:' + y + 'px;');
      this.domNode.style.left = x;
      this.domNode.style.top = y;
    };

    Popover.prototype.hide = function hide() {
      this.domNode.style.display = 'none';
      this.domNode.className = 'annotanium-popover';
    };

    Popover.prototype.show = function show() {
      this.domNode.style.display = 'block';
      this.domNode.className = 'annotanium-popover annotanium-popover--show';
    };

    Popover.prototype.isVisible = function isVisible() {
      return !!this.domNode.offsetParent;
    };

    return Popover;

  }).call(this);


  // Annotanium class
  // ==================================================

  var Annotanium = (function() {

    function Annotanium(imgNode, opts) {
      var defaults = {
        annotations: [],
        readOnly: false
      };

      opts = opts || {};

      this.opts = {
        annotations: opts.annotations || defaults.annotations,
        readOnly: opts.readOnly || defaults.readOnly
      };

      this.imgNode              = imgNode;
      this.imgLoaded            = false;
      this.canvas               = null;
      this.ctx                  = null;
      this.annotations          = [];
      this.queue                = [];
      this.addAnnotationPopover = null;
      this.listenersMap         = {};

      var img = new Image();

      img.onload = function() {
        this.imgLoaded = true;
        this.init.call(this);

        this.queue.forEach(function(annotation) {
          this.addAnnotation(annotation);
        }.bind(this));

        this.opts.annotations.forEach(function(annotation) {
          this.addAnnotation(annotation);
        }.bind(this));

        this.bindMouseListeners.call(this);
      }.bind(this);

      img.src = this.imgNode.src;
    }

    var mouseState = {
      dragStart:  null,
      dragEnd:    null,
      isDragging: false
    };

    Annotanium.prototype.init = function() {
      if (!support.dom) return false;

      this.width  = this.imgNode.clientWidth;
      this.height = this.imgNode.clientHeight;

      // Create wrapping div
      var div = document.createElement('div');
      div.className = 'annotanium';
      div.setAttribute('style', 'width:' + this.width + 'px;height:' + this.height + 'px;position:relative;');
      div.style.position = 'relative';
      div.style.width    = this.width;
      div.style.height   = this.height;
      this.wrapper = div;

      // Create canvas overlay
      this.canvas = utils.createHiDPICanvas(this.width, this.height);
      this.canvas.setAttribute('style', 'width:' + this.width + 'px;height:' + this.height + 'px;position:absolute;top:0;left:0;');
      this.canvas.style.position = 'absolute';
      this.canvas.style.top      = 0;
      this.canvas.style.left     = 0;

      // Create new annotation popover
      this.addAnnotationForm = new AddAnnotationForm();

      this.addAnnotationForm.onCancel(function(e) {
        e.preventDefault();
        this.addAnnotationPopover.hide();
        this.annotate();
      }.bind(this));

      this.addAnnotationForm.onSubmit(function(e) {
        e.preventDefault();

        var annotation = new Annotation(this.ctx);
        annotation.fromMouseEvents(mouseState.dragStart, mouseState.dragEnd);

        var annoObj = {
          position: {
            x: annotation.xPos,
            y: annotation.yPos
          },
          dimensions: {
            width: annotation.width,
            height: annotation.height
          },
          text: this.addAnnotationForm.getValue()
        };

        this.emit('annotation.create', annoObj);
        this.addAnnotation(annoObj);
        this.addAnnotationPopover.hide();
        this.annotate();
      }.bind(this));

      this.addAnnotationPopover = new Popover(this.addAnnotationForm.getDOMNode());

      // Wrap image
      utils.wrapHtml(div, this.imgNode);
      div.appendChild(this.canvas);
      div.appendChild(this.addAnnotationPopover.getDOMNode());

      _digestOptions.apply(this, [this.opts]);

      // Get canvas 2D context
      this.ctx = this.canvas.getContext('2d');

      // Set canvas context styles
      this.ctx.strokeStyle   = '#fff';
      this.ctx.fillStyle     = '#fff';
      this.ctx.shadowColor   = 'rgba(0,0,0,0.65)';
      this.ctx.shadowBlur    = 3;
      this.ctx.shadowOffsetX = 0;
      this.ctx.shadowOffsetY = 1;
    };

    Annotanium.prototype.updateSettings = function(newSettings) {
      for (var key in newSettings) {
        if (newSettings.hasOwnProperty(key)) {
          this.opts[key] = newSettings[key];
        }
      }

      if (this.imgLoaded) {
        _digestOptions.apply(this, [this.opts]);
        this.bindMouseListeners.call(this);
      }
    };

    Annotanium.prototype.annotate = function() {
      if (!this.imgLoaded) return false;

      this.ctx.clearRect(0, 0, this.width * utils.getDevicePixelRatio(), this.height * utils.getDevicePixelRatio());
      this.annotations.forEach(function(annotation) {
        annotation.draw();
      });
    };

    Annotanium.prototype.addAnnotation = function(annoObj) {
      if (this.imgLoaded) {
        var annotation = new Annotation(this.ctx);

        annotation.setMeta(annoObj);

        annotation.setRelPositionDimensions(
          annoObj.position.x,
          annoObj.position.y,
          annoObj.dimensions.width,
          annoObj.dimensions.height
        );

        annotation.setText(annoObj.text);
        annotation.draw();
        annotation.setId(annoObj.id);

        annotation.on('annotation.delete', _onAnnotationDelete.bind(this));

        this.annotations.push(annotation);

        this.annotate.call(this);
        this.emit('annotation.added', annoObj);
      } else {
        this.queue.push(annoObj);
      }
    };

    Annotanium.prototype.removeAnnotation = function removeAnnotation(annotationId) {
      var annotationIds = this.annotations.map(function(annotation) {
        return annotation.id;
      });

      var index = annotationIds.indexOf(annotationId);

      if (index > -1) {
        var annotation = this.annotations[index];
        annotation.destroy();
        this.annotations.splice(index, 1);
      }

      this.annotate.call(this);
    };

    Annotanium.prototype.removeAllAnnotations = function() {
      this.annotations = [];
      this.annotate();
    };

    Annotanium.prototype.showAnnotationPopover = function showAnnotationPopover(annotationId) {
      this.annotations.forEach(function(annotation) {
        if (annotation.id === annotationId) {
          annotation.textPopover.show();
        }
      });
    };

    Annotanium.prototype.hideAnnotationPopover = function hideAnnotationPopover(annotationId) {
      this.annotations.forEach(function(annotation) {
        if (annotation.id === annotationId) {
          annotation.textPopover.hide();
        }
      });
    };

    Annotanium.prototype.showAllAnnotationPopovers = function showAllAnnotationPopovers() {
      this.annotations.forEach(function(annotation) {
        annotation.textPopover.show();
      });
    };

    Annotanium.prototype.hideAllAnnotationPopovers = function hideAllAnnotationPopovers() {
      this.annotations.forEach(function(annotation) {
        annotation.textPopover.hide();
      });
    };

    Annotanium.prototype.bindMouseListeners = function() {
      // Clear
      this.canvas.removeEventListener('mousedown', this._onMouseDown);
      this.canvas.removeEventListener('mouseup', this._onMouseUp);
      this.canvas.removeEventListener('mousemove', this._onMouseMove);

      // Set
      this._onMouseDown = _onMouseDown.bind(this);
      this._onMouseUp = _onMouseUp.bind(this);
      this._onMouseMove = _onMouseMove.bind(this);

      // Bind
      if (!this.opts.readOnly) {
        this.canvas.addEventListener('mousedown', this._onMouseDown);
        this.canvas.addEventListener('mouseup', this._onMouseUp);
      }

      this.canvas.addEventListener('mousemove', this._onMouseMove);
    };

    Annotanium.prototype.on = function on(eventName, listener) {
      this.listenersMap[eventName] = this.listenersMap[eventName] || [];
      this.listenersMap[eventName].push(listener);
    };

    Annotanium.prototype.off = function off(eventName, listener) {
      var listeners = this.listenersMap[eventName];
      var index = listeners.indexOf(listener);
      if (index > -1) listeners.splice(index, 1);
    };

    Annotanium.prototype.emit = function emit(eventName, arg) {
      if (this.listenersMap[eventName]) {
        this.listenersMap[eventName].forEach(function(listener) {
          listener(arg);
        });
      }
    };


    function _digestOptions(opts) {
      if (opts.readOnly) {
        this.wrapper.className = 'annotanium annotanium--readOnly';
      } else {
        this.wrapper.className = 'annotanium';
      }
    }

    function _onMouseDown(e) {
      mouseState.dragStart = e;
      mouseState.isDragging = true;
      this.annotate();
      this.addAnnotationPopover.hide();
      this.hideAllAnnotationPopovers();
    }

    function _onMouseMove(e) {
      if (mouseState.isDragging) {
        mouseState.dragEnd = e;
        this.annotate();

        var annotation = new Annotation(this.ctx);
        annotation.fromMouseEvents(mouseState.dragStart, mouseState.dragEnd);
        annotation.draw(this.ctx);
      } else {
        if (this.addAnnotationPopover.isVisible()) return false;

        this.annotations.forEach(function(annotation) {
          if (annotation.isMouseOver(e, this.ctx)) {
            annotation.showTextPopover();
            return annotation;
          } else {
            annotation.hideTextPopover();
          }
        }.bind(this));
      }
    }

    function _onMouseUp(e) {
      mouseState.dragEnd = e;
      mouseState.isDragging = false;

      var annotation = new Annotation(this.ctx);
      annotation.fromMouseEvents(mouseState.dragStart, mouseState.dragEnd);

      if (annotation.width > 0.01 && annotation.height > 0.01) {
        this.addAnnotationForm.reset();
        this.addAnnotationPopover.setPosition(
          annotation.xPos * this.canvas.clientWidth,
          (annotation.yPos * this.canvas.clientHeight) + (annotation.height * this.canvas.clientHeight)
        );
        this.addAnnotationPopover.show();
        this.addAnnotationForm.focus();
      }
    }

    function _onAnnotationDelete(annotation) {
      this.removeAnnotation(annotation.id);
      this.emit('annotation.deleted', annotation);
    }

    return Annotanium;

  })();


  // Export
  // ========================================

  (function (root, factory) {
    if (typeof define === 'function' && define.amd) {
      define('Annotanium', [], factory);
    } else if (typeof exports === 'object') {
      module.exports = factory();
    } else {
      root.Annotanium = factory();
    }
  }(root, function () {
    return Annotanium;
  }));
})(this);
